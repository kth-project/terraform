################################################################################
# VPC
################################################################################

output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.vpc.vpc_id
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  value       = module.vpc.vpc_cidr_block
}

output "azs" {
  description = "A list of availability zones specified as argument to this module"
  value       = var.azs
}

output "name" {
  description = "The name of the VPC specified as argument to this module"
  value       = var.vpc_name
}

output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = var.private_subnets
}

output "private_subnets" {
  description = "List of IDs of private subnets"
  value       = var.public_subnets
}

output "igw_id" {
  description = "The ID of the Internet Gateway"
  value       = module.vpc.igw_id
}

output "nat_public_ips" {
  description = "List of public Elastic IPs created for AWS NAT Gateway"
  value       = module.vpc.nat_public_ips
}

################################################################################
# EC2
################################################################################

output "bastion_public_ip" {
  description = "The public IP address assigned to the instance, if applicable. NOTE: If you are using an aws_eip with your instance, you should refer to the EIP's address directly and not use `public_ip` as this field will change after the EIP is attached"
  value       = module.bastion.public_ip
}

output "k8s-master_private_ip" {
  description = "The private IP of the k8s-master EC2 instance"
  value       = module.k8s-master.private_ip
}

output "k8s-work1_private_ip" {
  description = "The private IP of the k8s-work1 EC2 instance"
  value       = module.k8s-work1.private_ip
}

output "k8s-work2_private_ip" {
  description = "The private IP of the k8s-work2 EC2 instance"
  value       = module.k8s-work2.private_ip
}


#################
# Security group  필요 시 추가 생성
#################
output "security_group_id" {
  description = "The ID of the security group"
  value       = module.k8s-sg.security_group_id
}


################################################################################
# Key Pair
################################################################################

output "key_name" {
  value       = var.key_name
  description = "The name of the generated key pair"
}
