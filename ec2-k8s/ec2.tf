# ###############################################################################
# EC2 Module
# ###############################################################################

module "bastion" {
  source = "../modules/terraform-aws-ec2-instance"

  name = "${var.ec2_name}-bastion"

  ami                         = var.ami
  instance_type               = var.instance_type
  availability_zone           = element(module.vpc.azs, 1)
  subnet_id                   = element(module.vpc.public_subnets, 1)
  associate_public_ip_address = true
  key_name                    = aws_key_pair.kth_key.key_name
  vpc_security_group_ids      = [module.bastion-sg.security_group_id]
  user_data = file("bastion.sh")

  enable_volume_tags = false
  root_block_device = [
    {
      encrypted   = true
      volume_type = "gp3"
      throughput  = 200
      volume_size = 8
      tags = {
        Name = "my-root-block"
      }
    },
  ]
}


module "k8s-master" {
  source = "../modules/terraform-aws-ec2-instance"

  name = "${var.ec2_name}-k8s-master"

  ami                         = var.ami
  instance_type               = var.k8s_instance_type
  availability_zone           = element(module.vpc.azs, 0)
  subnet_id                   = element(module.vpc.private_subnets, 0)
  associate_public_ip_address = false
  key_name                    = aws_key_pair.kth_key.key_name
  vpc_security_group_ids      = [module.k8s-sg.security_group_id]
  enable_volume_tags = false
  root_block_device = [
    {
      encrypted   = true
      volume_type = "gp3"
      throughput  = 200
      volume_size = 20
      tags = {
        Name = "my-root-block"
      }
    },
  ]
}

module "k8s-work1" {
  source = "../modules/terraform-aws-ec2-instance"

  name = "${var.ec2_name}-k8s-work1"

  ami                         = var.ami
  instance_type               = var.k8s_instance_type
  availability_zone           = element(module.vpc.azs, 0)
  subnet_id                   = element(module.vpc.private_subnets, 0)
  associate_public_ip_address = false
  key_name                    = aws_key_pair.kth_key.key_name
  vpc_security_group_ids      = [module.k8s-sg.security_group_id]
  enable_volume_tags = false
  root_block_device = [
    {
      encrypted   = true
      volume_type = "gp3"
      throughput  = 200
      volume_size = 20
      tags = {
        Name = "my-root-block"
      }
    },
  ]
}

module "k8s-work2" {
  source = "../modules/terraform-aws-ec2-instance"

  name = "${var.ec2_name}-k8s-work2"

  ami                         = var.ami
  instance_type               = var.k8s_instance_type
  availability_zone           = element(module.vpc.azs, 0)
  subnet_id                   = element(module.vpc.private_subnets, 0)
  associate_public_ip_address = false
  key_name                    = aws_key_pair.kth_key.key_name
  vpc_security_group_ids      = [module.k8s-sg.security_group_id]
  enable_volume_tags = false
  root_block_device = [
    {
      encrypted   = true
      volume_type = "gp3"
      throughput  = 200
      volume_size = 20
      tags = {
        Name = "my-root-block"
      }
    },
  ]
}

# ###############################################################################
# K8s Install 
# ###############################################################################

resource "null_resource" "copy_connect_private" {

  connection {
    host        = module.bastion.public_ip
    user        = "ubuntu"
    private_key = file("./kth-keypair.pem")
  }

  provisioner "file" {
    source      = "./NEW-k8s-master.sh"
    destination = "/home/ubuntu/NEW-k8s-master.sh"
  }

  provisioner "file" {
    source      = "./NEW-k8s-work.sh"
    destination = "/home/ubuntu/NEW-k8s-work.sh"
  }

  provisioner "file" {
    source      = "./kth-keypair.pem"
    destination = "/home/ubuntu/kth-keypair.pem"
  }
}


resource "null_resource" "k8s_master" {
  depends_on = [module.vpc]

  connection {
    bastion_host = module.bastion.public_ip
    host         = module.k8s-master.private_ip
    user         = "ubuntu"
    private_key  = file("./kth-keypair.pem")
  }

  provisioner "file" {
    source      = "./NEW-k8s-master.sh"
    destination = "/home/ubuntu/NEW-k8s-master.sh"
  }

  provisioner "file" {
    source      = "./NEW-k8s-work.sh"
    destination = "/home/ubuntu/NEW-k8s-work.sh"
  }

  provisioner "file" {
    source      = "./NEW-k8s-work02.sh"
    destination = "/home/ubuntu/NEW-k8s-work02.sh"
  }

  provisioner "file" {
    source      = "./kth-keypair.pem"
    destination = "/home/ubuntu/kth-keypair.pem"
  }

  provisioner "remote-exec" {
    script = "./NEW-k8s-master.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod 400 /home/ubuntu/kth-keypair.pem",
      "chmod 777 /home/ubuntu/NEW-k8s-master.sh",
      "scp -i /home/ubuntu/kth-keypair.pem -o StrictHostKeyChecking=no /home/ubuntu/NEW-k8s-master.sh ubuntu@${module.k8s-master.private_ip}:/home/ubuntu",
      "scp -i /home/ubuntu/kth-keypair.pem -o StrictHostKeyChecking=no /home/ubuntu/NEW-k8s-work.sh ubuntu@${module.k8s-work1.private_ip}:/home/ubuntu",
      "scp -i /home/ubuntu/kth-keypair.pem -o StrictHostKeyChecking=no /home/ubuntu/NEW-k8s-work02.sh ubuntu@${module.k8s-work2.private_ip}:/home/ubuntu",
      "scp -i /home/ubuntu/kth-keypair.pem -o StrictHostKeyChecking=no /home/ubuntu/join.sh ubuntu@${module.k8s-work1.private_ip}:/home/ubuntu",
      "scp -i /home/ubuntu/kth-keypair.pem -o StrictHostKeyChecking=no /home/ubuntu/join.sh ubuntu@${module.k8s-work2.private_ip}:/home/ubuntu"
    ]
  }
}

resource "null_resource" "k8s_work_01" {
  depends_on = [null_resource.k8s_master]

  connection {
    bastion_host = module.bastion.public_ip
    host         = module.k8s-work1.private_ip
    user         = "ubuntu"
    private_key  = file("./kth-keypair.pem")
  }

  provisioner "remote-exec" {
    script = "./NEW-k8s-work.sh"
  }
}

resource "null_resource" "k8s_work_02" {
  depends_on = [null_resource.k8s_master]

  connection {
    bastion_host = module.bastion.public_ip
    host         = module.k8s-work2.private_ip
    user         = "ubuntu"
    private_key  = file("./kth-keypair.pem")
  }

  provisioner "remote-exec" {
    script = "./NEW-k8s-work.sh"
  }
}
