##################################################################
# Application Load Balancer
##################################################################

module "alb" {
  source = "../modules/terraform-aws-alb"

  name = var.alb_name

  load_balancer_type = "application"
  vpc_id             = module.vpc.vpc_id
  subnets            = module.vpc.public_subnets
  security_groups    = [module.k8s-sg.security_group_id]
}
