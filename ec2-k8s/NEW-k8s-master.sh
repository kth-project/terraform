#!/bin/bash
# 패키지 업데이트 및 필수 도구 설치
sudo apt-get update -y
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

# Docker 설치
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
sudo echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update -y
sudo apt-get install -y docker-ce

# Containerd 설치
sudo apt-get install -y containerd
sudo mkdir -p /etc/containerd
sudo containerd config default | sudo tee /etc/containerd/config.toml
sudo sed -i 's/^disabled_plugins = \[\]/#disabled_plugins = \[\]/g' /etc/containerd/config.toml
sudo sed -i 's/SystemdCgroup = false/SystemdCgroup = true/g' /etc/containerd/config.toml
sudo systemctl restart containerd
sudo systemctl enable containerd

# 쿠버네티스 설치
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update -y
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl

# Master 노드 초기화
sudo swapoff -a
sudo kubeadm init --pod-network-cidr=10.244.0.0/16 --cri-socket "/run/containerd/containerd.sock"

# Kubernetes 홈 디렉토리 설정
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

sleep 1m

# # Flannel 네트워크 설치
kubectl apply -f https://github.com/weaveworks/weave/releases/download/v2.8.1/weave-daemonset-k8s.yaml

echo "모든 작업이 완료되었습니다. 다음 명령어를 사용하여 Worker 노드에서 조인 명령을 가져옵니다:"
echo "kubeadm token create --print-join-command"

JOIN_COMMAND=$(sudo kubeadm token create --print-join-command)
echo -e "#!/bin/bash\nsudo $JOIN_COMMAND" > /home/ubuntu/join.sh

# kubectl bash auto-completion을 설치합니다.
sudo apt-get update
sudo apt-get install -y bash-completion
source /usr/share/bash-completion/bash_completion

# kubectl과 연관된 자동 완성 기능을 활성화합니다.
echo 'source <(kubectl completion bash)' >>~/.bashrc

# 현재 셸에 적용될 자동 완성 활성화
source <(kubectl completion bash)

# 기본 kubectl context와 namespace를 위한 자동 완성을 설정합니다.
echo 'alias k=kubectl' >>~/.bashrc
echo 'source <(__k config use-context $(kubectl config current-context) 2>/dev/null)' >>~/.bashrc
source ~/.bashrc

echo "kubectl auto-completion 설정이 완료되었습니다."

