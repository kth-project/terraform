#############################################################
# Security group Module
#############################################################
module "bastion-sg" {
  source = "../modules/terraform-aws-security-group"

  name   = "${var.sg_name}-bastion-sg"
  vpc_id = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "External"
      cidr_blocks = "165.225.228.251/32"
    }
  ]

  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "External"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
}

module "k8s-sg" {
  source = "../modules/terraform-aws-security-group"

  name   = "${var.sg_name}-k8s-sg"
  vpc_id = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "Internal-VPC"
      cidr_blocks = "20.10.0.0/16"
    },
    {
      from_port   = 6443
      to_port     = 6443
      protocol    = "tcp"
      description = "External"
      cidr_blocks = "20.10.0.0/16"
    },
    {
      from_port   = 10250
      to_port     = 10250
      protocol    = "tcp"
      description = "kubelet"
      cidr_blocks = "20.10.0.0/16"
    },
    {
      from_port   = 2379
      to_port     = 2379
      protocol    = "tcp"
      description = "etcd"
      cidr_blocks = "20.10.0.0/16"
    },
    {
      from_port   = 2380
      to_port     = 2380
      protocol    = "tcp"
      description = "etcd"
      cidr_blocks = "20.10.0.0/16"
    },
    {
      from_port   = 0
      to_port     = 65535
      protocol    = "tcp"
      description = "all ports"
      cidr_blocks = "20.10.0.0/16"
    }
  ]

  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "External"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
}
