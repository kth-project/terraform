################################################################################
# VPC
################################################################################

output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.vpc.vpc_id
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  value       = module.vpc.vpc_cidr_block
}

output "azs" {
  description = "A list of availability zones specified as argument to this module"
  value       = var.azs
}

output "name" {
  description = "The name of the VPC specified as argument to this module"
  value       = var.vpc_name
}

output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = var.private_subnets
}

output "private_subnets" {
  description = "List of IDs of private subnets"
  value       = var.public_subnets
}

output "igw_id" {
  description = "The ID of the Internet Gateway"
  value       = module.vpc.igw_id
}

output "nat_public_ips" {
  description = "List of public Elastic IPs created for AWS NAT Gateway"
  value       = module.vpc.nat_public_ips
}

################################################################################
# EC2
################################################################################

output "bastion_public_ip" {
  description = "The public IP address assigned to the instance, if applicable. NOTE: If you are using an aws_eip with your instance, you should refer to the EIP's address directly and not use `public_ip` as this field will change after the EIP is attached"
  value       = module.bastion.public_ip
}

# output "k8s-master_private_ip" {
#   description = "The private IP of the k8s-master EC2 instance"
#   value       = module.k8s-master.private_ip
# }

# output "k8s-work1_private_ip" {
#   description = "The private IP of the k8s-work1 EC2 instance"
#   value       = module.k8s-work1.private_ip
# }

# output "k8s-work2_private_ip" {
#   description = "The private IP of the k8s-work2 EC2 instance"
#   value       = module.k8s-work2.private_ip
# }


#################
# Security group  필요 시 추가 생성
#################
output "security_group_id" {
  description = "The ID of the security group"
  value       = module.k8s-sg.security_group_id
}


################################################################################
# Key Pair
################################################################################

output "key_name" {
  value       = var.key_name
  description = "The name of the generated key pair"
}

################################################################################
# Route53 Module
################################################################################
# output "route53_zone_name" {
#   description = "Name of Route53 zone"
#   value       = module.zones.route53_zone_name
# }

# output "route53_zone_zone_id" {
#   description = "Zone ID of Route53 zone"
#   value       = module.zones.route53_zone_zone_id
# }

# ###############################################################################
# EKS Module
# ###############################################################################

# output "cluster_arn" {
#   description = "The Amazon Resource Name (ARN) of the cluster"
#   value       = module.eks.cluster_arn
# }

# output "cluster_certificate_authority_data" {
#   description = "Base64 encoded certificate data required to communicate with the cluster"
#   value       = module.eks.cluster_certificate_authority_data
# }

# output "cluster_endpoint" {
#   description = "Endpoint for your Kubernetes API server"
#   value       = module.eks.cluster_endpoint
# }

# output "cluster_id" {
#   description = "The ID of the EKS cluster. Note: currently a value is returned only for local EKS clusters created on Outposts"
#   value       = module.eks.cluster_id
# }

# output "cluster_name" {
#   description = "The name of the EKS cluster"
#   value       = module.eks.cluster_name
# }

# output "cluster_platform_version" {
#   description = "Platform version for the cluster"
#   value       = module.eks.cluster_platform_version
# }

# output "cluster_status" {
#   description = "Status of the EKS cluster. One of `CREATING`, `ACTIVE`, `DELETING`, `FAILED`"
#   value       = module.eks.cluster_status
# }

# output "cluster_security_group_id" {
#   description = "Cluster security group that was created by Amazon EKS for the cluster. Managed node groups use this security group for control-plane-to-data-plane communication. Referred to as 'Cluster security group' in the EKS console"
#   value       = module.eks.cluster_security_group_id
# }

# output "cluster_security_group_arn" {
#   description = "Amazon Resource Name (ARN) of the cluster security group"
#   value       = module.eks.cluster_security_group_arn
# }

# output "cluster_iam_role_name" {
#   description = "IAM role name of the EKS cluster"
#   value       = module.eks.cluster_iam_role_name
# }

# output "cluster_iam_role_arn" {
#   description = "IAM role ARN of the EKS cluster"
#   value       = module.eks.cluster_iam_role_arn
# }

# output "cluster_iam_role_unique_id" {
#   description = "Stable and unique string identifying the IAM role"
#   value       = module.eks.cluster_iam_role_unique_id
# }

# output "eks_managed_node_groups" {
#   description = "Map of attribute maps for all EKS managed node groups created"
#   value       = module.eks.eks_managed_node_groups
# }

# output "eks_managed_node_groups_autoscaling_group_names" {
#   description = "List of the autoscaling group names created by EKS managed node groups"
#   value       = module.eks.eks_managed_node_groups_autoscaling_group_names
# }

