# ###############################################################################
# EC2 Module
# ###############################################################################

module "bastion" {
  source = "../modules/terraform-aws-ec2-instance"

  name = "${var.ec2_name}-bastion"

  ami                         = var.ami
  instance_type               = var.instance_type
  availability_zone           = element(module.vpc.azs, 1)
  subnet_id                   = element(module.vpc.public_subnets, 1)
  associate_public_ip_address = true
  key_name                    = aws_key_pair.kth_key.key_name
  vpc_security_group_ids      = [module.bastion-sg.security_group_id]
  user_data                   = file("bastion.sh")

  enable_volume_tags = false
  root_block_device = [
    {
      encrypted   = true
      volume_type = "gp3"
      throughput  = 200
      volume_size = 50
      tags = {
        Name = "kth"
      }
    },
  ]
}


# bastion_copy
resource "null_resource" "bastion_copy" {
  connection {
    user        = "ubuntu"
    type        = "ssh"
    host        = module.bastion.public_ip
    private_key = file("./kth-keypair.pem")
    timeout     = "10m"
  }

  provisioner "file" {
    source      = "./argocd-ingress.yaml"
    destination = "/home/ubuntu/argocd-ingress.yaml"
  }
  provisioner "file" {
    source      = "./nginx-ingress.yaml"
    destination = "/home/ubuntu/nginx-ingress.yaml"
  }
}