################################################################################
# VPC Module
################################################################################

module "vpc" {
  source = "../modules/terraform-aws-vpc"

  name = var.vpc_name
  cidr = var.vpc_cidr

  azs = var.azs

  private_subnet_names = var.private_subnet_names
  private_subnets      = var.private_subnets

  public_subnet_names    = var.public_subnet_names
  public_subnets         = var.public_subnets
  enable_dns_hostnames   = true
  enable_dhcp_options    = true
  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false
  create_igw             = true
}