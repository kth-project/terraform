# provider "kubernetes" {
#   alias                  = "second"
#   host                   = module.eks_2.cluster_endpoint
#   cluster_ca_certificate = base64decode(module.eks_2.cluster_certificate_authority_data)

#   exec {
#     api_version = "client.authentication.k8s.io/v1beta1"
#     command     = "aws"
#     # This requires the awscli to be installed locally where Terraform is executed
#     args = ["eks", "get-token", "--cluster-name", module.eks_2.cluster_name]
#   }
# }

# data "aws_availability_zones" "available2" {}
# data "aws_caller_identity" "current2" {}


# locals {
#   name2 = var.cluster_name_2

#   tags2 = {
#     Example    = local.name2
#     GithubRepo = "terraform-aws-eks"
#     GithubOrg  = "terraform-aws-modules"
#   }
# }

# resource "aws_iam_policy" "additional-2" {
#   name = "${local.name2}-additional"

#   tags = {
#     Example    = local.name
#     GithubRepo = "terraform-aws-eks"
#     GithubOrg  = "terraform-aws-modules"
#   }

#   policy = jsonencode({
#     Version = "2012-10-17"
#     Statement = [
#       {
#         Action = [
#           "ec2:Describe*",
#           "ec2:CreateVolume",
#           "logs:CreateLogStream",
#           "ec2:CreateTags",
#           "ec2:AttachVolume",
#         ]
#         Effect   = "Allow"
#         Resource = "*"
#       },
#     ]
#   })
# }

# ################################################################################
# # EKS Module-2
# ################################################################################

# module "eks_2" {
#   source = "../modules/terraform-aws-eks"

#   cluster_name    = var.cluster_name_2
#   cluster_version = var.cluster_version

#   cluster_endpoint_public_access = false

#   cluster_addons = {
#     coredns = {
#       most_recent = true
#     }
#     kube-proxy = {
#       most_recent = true
#     }
#     vpc-cni = {
#       most_recent = true
#     }
#     aws-ebs-csi-driver = {
#       most_recent = true
#     }
#     aws-efs-csi-driver = {
#       most_recent = true
#     }
#     amazon-cloudwatch-observability = {
#       most_recent = true
#     }
#   }

#   vpc_id                   = module.vpc.vpc_id
#   subnet_ids               = module.vpc.private_subnets
#   control_plane_subnet_ids = module.vpc.intra_subnets

#   # manage_aws_auth_configmap = false

#   cluster_security_group_additional_rules = {
#     igress_nodes_ephemeral_ports_tcp = {
#       description = ""
#       protocol    = -1
#       from_port   = 0
#       to_port     = 0
#       type        = "ingress"
#       cidr_blocks = ["0.0.0.0/0"]
#     }
#     kubectl = {
#       description = "gitlab server"
#       from_port   = 443
#       protocol    = "tcp"
#       type        = "ingress"
#       to_port     = 443
#       cidr_blocks = ["20.10.0.0/16"]
#     }
#     kubectl2 = {
#       description = "gitlab server"
#       from_port   = 80
#       protocol    = "tcp"
#       type        = "ingress"
#       to_port     = 80
#       cidr_blocks = ["20.10.0.0/16"]
#     }
#     kubectl3 = {
#       description = "Argocd server"
#       from_port   = 80
#       protocol    = "tcp"
#       type        = "ingress"
#       to_port     = 80
#       cidr_blocks = ["0.0.0.0/0"]
#     }
#     kubectl4 = {
#       description = "Argocd server"
#       from_port   = 443
#       protocol    = "tcp"
#       type        = "ingress"
#       to_port     = 443
#       cidr_blocks = ["0.0.0.0/0"]
#     }
#     kubectl5 = {
#       description = "loki server"
#       from_port   = 3100
#       protocol    = "tcp"
#       type        = "ingress"
#       to_port     = 3100
#       cidr_blocks = ["20.10.0.0/16"]
#     }
#   }

#   iam_role_additional_policies = {
#     additional = aws_iam_policy.additional-2.arn
#   }

#   # EKS Managed Node Group(s)
#   eks_managed_node_group_defaults = {
#     ami_type                              = "AL2_x86_64"
#     attach_cluster_primary_security_group = true
#     vpc_security_group_ids                = [module.k8s-sg.security_group_id]
#     iam_role_additional_policies = {
#       additional = aws_iam_policy.additional-2.arn
#     }
#   }

#   eks_managed_node_groups = {
#     kth_node_2 = {
#       min_size     = var.min_size
#       max_size     = var.max_size
#       desired_size = var.desired_size
#       key_name     = aws_key_pair.kth_key.key_name

#       instance_types = ["t3.large"]
#       capacity_type  = "ON_DEMAND"
#       labels = {
#         Environment = "test"
#         GithubRepo  = "terraform-aws-eks"
#         GithubOrg   = "terraform-aws-modules"
#       }

#       update_config = {
#         max_unavailable_percentage = 33 # or set `max_unavailable`
#       }

#       tags2 = {
#         ExtraTag = "example"
#       }
#     }
#   }

# }