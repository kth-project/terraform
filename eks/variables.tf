


################################################################################
# VPC
################################################################################
variable "vpc_name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
  default     = "kth"
}

variable "vpc_cidr" {
  description = "(Optional) The IPv4 CIDR block for the VPC. CIDR can be explicitly set or it can be derived from IPAM using `ipv4_netmask_length` & `ipv4_ipam_pool_id`"
  type        = string
  default     = "20.10.0.0/16"
}

variable "azs" {
  description = "A list of availability zones names or ids in the region"
  type        = list(string)
  default     = ["us-east-2a", "us-east-2c"]
}

################################################################################
# Publiс Subnets
################################################################################

variable "public_subnets" {
  description = "A list of public subnets inside the VPC"
  type        = list(string)
  default     = ["20.10.15.0/24", "20.10.10.0/24"]
}

variable "public_subnet_names" {
  description = "Explicit values to use in the Name tag on public subnets. If empty, Name tags are generated"
  type        = list(string)
  default     = ["kth-pub-15.0", "kth-pub-10.0"]
}

################################################################################
# Private Subnets
################################################################################

variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  type        = list(string)
  default     = ["20.10.25.0/24", "20.10.20.0/24"]
}

variable "private_subnet_names" {
  description = "Explicit values to use in the Name tag on private_subnets. If empty, Name tags are generated"
  type        = list(string)
  default     = ["kth-pri-25.0", "kth-pri-20.0"]
}

################################################################################
# Internet Gateway
################################################################################

variable "create_igw" {
  description = "Controls if an Internet Gateway is created for public subnets and the related routes that connect them"
  type        = bool
  default     = true
}

################################################################################
# EC2 
################################################################################
variable "ec2_name" {
  description = "Name to be used on EC2 instance created"
  type        = string
  default     = "kth"
}

variable "ami" {
  description = "ID of AMI to use for the instance"
  type        = string
  default     = "ami-05fb0b8c1424f266b"
}

variable "instance_type" {
  description = "The type of instance to start"
  type        = string
  default     = "t3.micro"
}

variable "k8s_instance_type" {
  description = "The type of instance to start"
  type        = string
  default     = "t3.medium"
}

#################
# Security group
#################
variable "sg_name" {
  description = "Name of security group - not required if create_sg is false"
  type        = string
  default     = "kth"
}

################################################################################
# Key Pair
################################################################################
variable "key_name" {
  description = "The name for the key pair. Conflicts with `key_name_prefix`"
  type        = string
  default     = "kth-keypair"
}

################################################################################
# Cluster
################################################################################

variable "cluster_name" {
  description = "Name of the EKS cluster"
  type        = string
  default     = "kth"
}

# variable "cluster_name_2" {
#   description = "Name of the EKS cluster"
#   type        = string
#   default     = "kth-2"
# }

variable "cluster_version" {
  description = "Kubernetes `<major>.<minor>` version to use for the EKS cluster (i.e.: `1.27`)"
  type        = string
  default     = 1.28
}

variable "min_size" {
  description = "Minimum number of instances/nodes"
  type        = number
  default     = 2
}

variable "max_size" {
  description = "Maximum number of instances/nodes"
  type        = number
  default     = 3
}

variable "desired_size" {
  description = "Desired number of instances/nodes"
  type        = number
  default     = 2
}

