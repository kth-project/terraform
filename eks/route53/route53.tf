locals {
  zone_name = sort(keys(module.zones.route53_zone_zone_id))[0]
  zone_id   = module.zones.route53_zone_zone_id["taehyeon.site"]
}

module "zones" {
  source = "../../modules/terraform-aws-route53/modules/zones"

  zones = {
    "taehyeon.site" = {
      comment = "taehyeon.site"
      tags = {
        Name = "taehyeon.site"
      }
    }
  }
  tags = {
    ManagedBy = "Terraform"
  }
}