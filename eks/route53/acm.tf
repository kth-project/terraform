
# ##########################################################
# # ACM Module
# ##########################################################
module "acm" {
  source = "../../modules/terraform-aws-acm"

  providers = {
    # aws.acm = aws
    # aws.dns = aws
    aws = aws
  }

  domain_name = module.zones.route53_zone_name["taehyeon.site"]
  zone_id     = module.zones.route53_zone_zone_id["taehyeon.site"]

  subject_alternative_names = [
    "*.${module.zones.route53_zone_name["taehyeon.site"]}"
  ]

  tags = {
    Name = module.zones.route53_zone_name["taehyeon.site"]
  }
}